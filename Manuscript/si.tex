%\documentclass[aps,prb,reprint,showkeys,superscriptaddress]{revtex4-1}
\documentclass[reprint,noshowkeys,superscriptaddress]{revtex4-1}
\usepackage{bm,graphicx,tabularx,array,booktabs,dcolumn,xcolor,microtype,multirow,amscd,amsmath,amssymb,amsfonts,physics,siunitx,enumitem}
\usepackage[version=4]{mhchem}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{txfonts}
\usepackage[normalem]{ulem}
\usepackage{mleftright}

\bibliographystyle{achemso}
\AtBeginDocument{\nocite{achemso-control}}

\usepackage{tikz}
\usetikzlibrary{arrows.meta,positioning,shapes.misc,arrows}


\renewcommand{\thepage}{\arabic{page}} 
\renewcommand{\thesection}{S\arabic{section}}  
\renewcommand{\thesubsection}{S\arabic{section}.\arabic{subsection}}  
\renewcommand{\thetable}{S\arabic{table}}  
\renewcommand{\thefigure}{S\arabic{figure}} 
\renewcommand{\theequation}{S\arabic{equation}} 

%%% Definition of colors for personal remark %%%
\definecolor{hughgreen}{RGB}{100, 0, 140}
\newcommand{\hugh}[1]{\textcolor{hughgreen}{#1}}
\newcommand{\todo}[1]{\textcolor{red}{#1}}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\ant}[1]{\textcolor{orange}{#1}}
\newcommand{\trashant}[1]{\textcolor{orange}{\sout{#1}}}

\usepackage{titlesec}
\titlespacing\section{10pt plus 2pt minus 2pt}{10pt plus 2pt minus 2pt}{10pt plus 2pt minus 2pt}
\titlespacing\subsection{2pt plus 2pt minus 2pt}{10pt plus 2pt minus 2pt}{5pt plus 2pt minus 2pt}
\titlespacing\subsubsection{2pt plus 2pt minus 2pt}{10pt plus 2pt minus 2pt}{5pt plus 2pt minus 2pt}

\allowdisplaybreaks

\usepackage[
	colorlinks=true,
    citecolor=blue,
    linkcolor=blue,
    filecolor=blue,      
    urlcolor=blue,	
    breaklinks=true
	]{hyperref}
\urlstyle{same}

%============================================================%
%%% NEWCOMMANDS %%%
%============================================================%


\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\cfoot{S\thepage}

% Uprights
\newcommand{\e}{\mathrm{e}}
\newcommand{\Eh}{\mathrm{E_h}}
\newcommand{\s}{\mathrm{s}}
\newcommand{\tS}{\mathrm{S}}
\newcommand{\tT}{\mathrm{T}}
\newcommand{\tc}{\text{c}}
\newcommand{\tto}{\text{o}}

\usepackage{upgreek}
\newcommand{\sigg}{{\upsigma_\textrm{g}}}
\newcommand{\sigu}{{\upsigma_\textrm{u}}}

\newcommand{\Nbas}{n}
\newcommand{\Ndet}{M}
\newcommand{\Ne}{N}

% Bold symbols
\newcommand{\bC}{\bm{C}}
\newcommand{\bc}{\bm{c}}
\newcommand{\br}{\bm{r}}
\newcommand{\bx}{\bm{x}}
\newcommand{\bR}{\bm{R}}
\newcommand{\bS}{\bm{S}}

%%% Matrix %%%
\newcommand{\FC}{F^{\text{C}}} % Core Fock matrix
\newcommand{\FA}{F^{\text{A}}} % Active Fock matrix

\newcommand{\rt}{\rho}

\newcommand{\mc}{\multicolumn}
\newcommand{\fnm}{\footnotemark}
\newcommand{\fnt}{\footnotetext}
\newcommand{\mcc}[1]{\multicolumn{1}{c}{#1}}

% Phantom dash
\newcommand{\hd}{\hphantom{-}}

% addresses
\newcommand{\LCPQ}{Laboratoire de Chimie et Physique Quantiques (UMR 5626), Universit\'e de Toulouse, CNRS, UPS, France}

\begin{document}	

\title{\large A similarity renormalization group approach to Green's function methods}
%\title{Excited states, symmetry breaking, and unphysical solutions in CASSCF theory}
%\title{Characterising state-specific CASSCF theory: Excited states, symmetry breaking, and unphysical solutions}
%\title{Exploring the CASSCF energy landscape: Excited states, symmetry breaking, and unphysical solutions}

\author{Antoine \surname{Marie} \textsuperscript{*} }
	\email{amarie@irsamc.ups-tlse.fr}
	\affiliation{\LCPQ}

\author{Pierre-Fran\c{c}ois \surname{Loos}}
	\email{loos@irsamc.ups-tlse.fr}
	\affiliation{\LCPQ}

\begin{abstract}
\end{abstract}

\maketitle
\thispagestyle{fancy}
\linepenalty1000
\raggedbottom
\onecolumngrid

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{SRG-$G_0W_0$, ev$GW$ and SRG-ev$GW$ statistics}
\label{app:appendixA}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


In this section, the values obtained with the two alternative SRG-based methods derived in the main manuscript, SRG-$G_0W_0$ and SRG-ev$GW$, are reported along with their corresponding histogram plot of the errors.
For the sake of completeness, the SRG-regularized self-energy and quasiparticle equation used for the SRG-$G_0W_0$ and SRG-ev$GW$ calculations are reported below:
\begin{equation}
  \epsilon_p^{\text{HF}} + \Sigma^\text{SRG-$GW$}_{pp}(\omega)  - \omega = 0,
\end{equation}
with
\begin{equation}
  \Sigma^\text{SRG-$GW$}_{pp}(\omega) = \sum_{i\nu} \frac{(W_{pi}^{\nu})^2 }{\omega - \epsilon_i + \Omega_{\nu}}e^{-2(\epsilon_p - \epsilon_i + \Omega_{\nu})^2 s} + \sum_{a\nu} \frac{(W_{pa}^{\nu})^2}{\omega - \epsilon_a - \Omega_{\nu}}e^{-2(\epsilon_p - \epsilon_a - \Omega_{\nu})^2 s},
\end{equation}
Therefore, the SRG-$G_0W_0$ values are obtained by solving once these equations (one-shot procedure) without linearization, while the SRG-ev$GW$ results correspond to solutions of these equations where self-consistency on the $\epsilon_p$'s has been reached.

One observe in Table \ref{tab:tab1} that the $G_0W_0$ and SRG-$G_0W_0$ values are the same for all systems (up to $\num{e-2}$\si{\electronvolt}).
Figure \ref{fig:supporting} shows that ev$GW$ provides a slight improvement over $G_0W_0$, while ev$GW$ and SRG-ev$GW$ perform similarly.
One interesting fact is that the convergence of SRG-ev$GW$ deteriorates faster than for SRG-qs$GW$ with respect to $s$.
We suspect that it is due to the absence of the off-diagonal terms.

%%% FIG 1 %%% 
\begin{figure*}[h]
  \includegraphics[width=\linewidth]{supporting}
  \caption{
    Histogram of the errors [with respect to $\Delta$CCSD(T)] for the principal IP of the $GW$50 test set calculated using $G_0W_0$@HF, SRG-$G_0W_0$@HF, ev$GW$, and SRG-ev$GW$.
    All calculations are performed with the aug-cc-pVTZ basis.
    \label{fig:supporting}}
\end{figure*}
%%% %%% %%% %%%
%%% FIG 2 %%% 
\begin{figure*}[h]
  \includegraphics[width=\linewidth]{supporting2}
  \caption{
    Histogram of the errors [with respect to $\Delta$CCSD(T)] for the principal EA of the $GW$50 test set calculated using $G_0W_0$@HF, SRG-$G_0W_0$@HF, ev$GW$, and SRG-ev$GW$.
    All calculations are performed with the aug-cc-pVTZ basis.
    \label{fig:supporting}}
\end{figure*}
%%% %%% %%% %%%
%%% TABLE I %%%
\begin{table*}
  \caption{Principal IP and EA (in eV) of the $GW$50 test set calculated using $\Delta$CCSD(T) (reference), $G_0W_0$@HF, SRG-$G_0W_0$@HF, ev$GW$, and SRG-ev$GW$. 
  The statistical descriptors associated with the errors with respect to the reference values are also reported.
  All calculations are performed with the aug-cc-pVTZ basis.}
  \label{tab:tab1}
  \begin{ruledtabular}
    \begin{tabular}{ldddddddddd}
    & \mc{5}{c}{Principal IP} & \mc{5}{c}{Principal EA} \\
    \cline{2-6} \cline{7-11}
    & \mcc{$\Delta\text{CCSD(T)}$} & \mcc{$G_0W_0$@HF} & \mcc{SRG-$G_0W_0$@HF} & \mcc{ev$GW$} & \mcc{SRG-ev$GW$} & \mcc{$\Delta\text{CCSD(T)}$} & \mcc{$G_0W_0$@HF} & \mcc{SRG-$G_0W_0$@HF} & \mcc{ev$GW$} & \mcc{SRG-ev$GW$} \\
    Mol.  &   \mcc{(Ref.)} & \mcc{($\eta=\num{e-3}$)} & \mcc{($s=\num{e3}$)} & \mcc{($\eta=\num{e-1}$)} & \mcc{($s=\num{50}$)} & \mcc{(Ref.)} &  \mcc{($\eta=\num{e-3}$)} &  \mcc{($s=\num{e3}$)}  & \mcc{($\eta=\num{e-1}$)} & \mcc{($s=\num{50}$)} \\
    \hline
    \ce{He}     & 24.54 & 24.59 & 24.59 & 24.58 & 24.57 & -2.66 & -2.66 & -2.66 & -2.66 & -2.66 \\
    \ce{Ne}     & 21.47 & 21.46 & 21.46 & 21.30 & 21.29 & -5.09 & -5.25 & -5.25 & -5.24 & -5.24\\
    \ce{H2}     & 16.40 & 16.49 & 16.49 & 16.52 & 16.51 & -1.35 & -1.28 & -1.28 & -1.28 & -1.28 \\
    \ce{Li2}    &  5.25 &  5.38 &  5.38 &  5.44 &  5.42 &  0.34 &  0.17 &  0.17 &  0.16 &  0.17 \\
    \ce{LiH}    &  8.02 &  8.22 &  8.22 &  8.26 &  8.23 & -0.29 &  0.27 &  0.27 &  0.27 &  0.27 \\
    \ce{HF}     & 16.15 & 16.25 & 16.25 & 16.10 & 16.09 & -0.66 & -0.71 & -0.71 & -0.71 & -0.71 \\
    \ce{Ar}     & 15.60 & 15.72 & 15.72 & 15.67 & 15.66 & -2.55 & -2.68 & -2.68 & -2.67 & -2.67 \\
    \ce{H2O}    & 12.69 & 12.90 & 12.90 & 12.80 & 12.79 & -0.61 & -0.68 & -0.68 & -0.68 & -0.68 \\
    \ce{LiF}    & 11.47 & 11.40 & 11.40 & 11.20 & 11.18 &  0.35 &  0.33 &  0.33 &  0.33 &  0.33 \\
    \ce{HCl}    & 12.67 & 12.78 & 12.78 & 12.76 & 12.75 & -0.57 & -0.64 & -0.64 & -0.64 & -0.64 \\
    \ce{BeO}    &  9.95 &  9.74 &  9.74 &  9.64 &  9.61 &  2.17 &  2.28 &  2.28 &  2.30 &  2.31 \\
    \ce{CO}     & 13.99 & 14.80 & 14.80 & 14.77 & 14.76 & -1.57 & -1.66 & -1.66 & -1.65 & -1.65 \\
    \ce{N2}     & 15.54 & 17.10 & 17.10 & 17.10 & 17.09 & -2.37 & -2.10 & -2.10 & -2.10 & -2.10 \\
    \ce{CH4}    & 14.39 & 14.76 & 14.76 & 14.76 & 14.75 & -0.65 & -0.70 & -0.70 & -0.69 & -0.69 \\
    \ce{BH3}    & 13.31 & 13.68 & 13.68 & 13.70 & 13.69 & -0.09 & -0.46 & -0.46 & -0.46 & -0.45 \\
    \ce{NH3}    & 10.91 & 11.22 & 11.22 & 11.19 & 11.17 & -0.61 & -0.68 & -0.68 & -0.68 & -0.68 \\
    \ce{BF}     & 11.15 & 11.34 & 11.34 & 11.37 & 11.36 & -0.80 & -0.90 & -0.90 & -0.90 & -0.90 \\
    \ce{BN}     & 12.05 & 11.76 & 11.76 & 11.78 & 11.76 &  3.02 &  3.90 &  3.90 &  3.95 &  3.95 \\
    \ce{SH2}    & 10.39 & 10.51 & 10.51 & 10.51 & 10.50 & -0.52 & -0.60 & -0.60 & -0.60 & -0.60 \\
    \ce{F2}     & 15.81 & 16.35 & 16.35 & 16.15 & 16.14 &  0.32 & -0.53 & -0.53 & -0.47 & -0.47 \\
    \ce{MgO}    &  7.97 &  8.40 &  8.40 &  8.34 &  8.28 &  1.54 &  1.64 &  1.64 &  1.65 &  1.66 \\
    \ce{O3}     & 12.85 & 13.56 & 13.56 & 13.53 & 13.51 &  1.82 &  2.19 &  2.19 &  2.25 &  2.25 \\
    \ce{C2H2}   & 11.45 & 11.57 & 11.57 & 11.60 & 11.59 & -0.80 & -0.71 & -0.71 & -0.71 & -0.71 \\
    \ce{HCN}    & 13.76 & 13.86 & 13.86 & 13.87 & 13.86 & -0.53 & -0.52 & -0.52 & -0.52 & -0.52 \\
    \ce{B2H6}   & 12.27 & 12.81 & 12.81 & 12.81 & 12.80 & -0.52 & -0.56 & -0.56 & -0.56 & -0.56 \\
    \ce{CH2O}   & 10.93 & 11.39 & 11.39 & 11.34 & 11.32 & -0.60 & -0.61 & -0.61 & -0.60 & -0.60 \\
    \ce{C2H4}   & 10.69 & 10.74 & 10.74 & 10.78 & 10.77 & -1.90 & -0.75 & -0.75 & -0.74 & -0.74 \\
    \ce{SiH4}   & 12.79 & 13.22 & 13.22 & 13.23 & 13.22 & -0.53 & -0.59 & -0.59 & -0.59 & -0.59 \\
    \ce{PH3}    & 10.60 & 10.79 & 10.79 & 10.82 & 10.81 & -0.51 & -0.58 & -0.58 & -0.58 & -0.58 \\
    \ce{CH4O}   & 11.09 & 11.55 & 11.55 & 11.48 & 11.47 & -0.59 & -0.64 & -0.64 & -0.64 & -0.64 \\
    \ce{H2NNH2} &  9.49 &  9.84 &  9.84 &  9.80 &  9.79 & -0.60 & -0.69 & -0.69 & -0.68 & -0.68 \\
    \ce{HOOH}   & 11.51 & 11.96 & 11.96 & 11.85 & 11.83 & -0.96 & -0.75 & -0.75 & -0.75 & -0.75 \\
    \ce{KH}     &  6.32 &  6.44 &  6.44 &  6.48 &  6.42 &  0.30 &  0.28 &  0.28 &  0.28 &  0.28 \\
    \ce{Na2}    &  4.93 &  4.98 &  4.98 &  5.03 &  5.02 &  0.36 &  0.26 &  0.26 &  0.24 &  0.26 \\
    \ce{HN3}    & 10.77 & 11.12 & 11.12 & 11.11 & 11.10 & -0.51 &  -0.6 &  -0.6 & -0.59 & -0.59 \\
    \ce{CO2}    & 13.80 & 14.24 & 14.24 & 14.16 & 14.15 & -0.88 & -0.98 & -0.98 & -0.97 & -0.97 \\
    \ce{PN}     & 11.90 & 12.33 & 12.33 & 12.34 & 12.33 & -0.02 & -0.03 & -0.03 &  0.01 &  0.01 \\
    \ce{CH2O2}  & 11.54 & 12.00 & 12.00 & 11.90 & 11.88 & -0.63 & -0.69 & -0.69 & -0.68 & -0.68 \\
    \ce{C4}     & 11.43 & 11.77 & 11.77 & 11.77 & 11.76 &  2.38 &  2.24 &  2.24 &  2.34 &  2.35 \\
    \ce{C3H6}   & 10.83 & 11.20 & 11.20 & 11.20 & 11.19 & -0.94 & -0.75 & -0.75 & -0.75 & -0.75 \\
    \ce{C2H3F}  & 10.63 & 10.84 & 10.84 & 10.85 & 10.84 & -0.65 & -0.69 & -0.69 & -0.68 & -0.68 \\
    \ce{C2H4O}  & 10.29 & 10.84 & 10.84 & 10.76 & 10.75 & -0.54 & -0.56 & -0.56 & -0.56 & -0.56 \\
    \ce{C2H6O}  & 10.82 & 11.37 & 11.37 & 11.31 & 11.30 & -0.58 & -0.65 & -0.65 & -0.64 & -0.64 \\
    \ce{C3H8}   & 12.13 & 12.61 & 12.61 & 12.60 & 12.59 & -0.63 & -0.70 & -0.70 & -0.70 & -0.70 \\
    \ce{NaCl}   &  9.10 &  9.20 &  9.20 &  9.16 &  9.13 &  0.67 &  0.64 &  0.64 &  0.64 &  0.64 \\
    \ce{P2}     & 10.72 & 10.49 & 10.49 & 10.52 & 10.51 &  0.43 &  0.47 &  0.47 &  0.53 &  0.54 \\
    \ce{MgF2}   & 13.93 & 13.94 & 13.94 & 13.74 & 13.72 &  0.29 &  0.15 &  0.15 &  0.16 &  0.16 \\
    \ce{OCS}    & 11.23 & 11.52 & 11.52 & 11.50 & 11.49 & -1.43 & -1.03 & -1.03 & -1.02 & -1.01 \\
    \ce{SO2}    & 10.48 & 11.38 & 11.38 & 11.34 & 11.33 &  2.24 &  2.82 &  2.82 &  2.87 &  2.88 \\
    \ce{C2H3Cl} & 10.17 & 10.39 & 10.39 & 10.39 & 10.38 & -0.61 & -0.66 & -0.66 & -0.65 & -0.65 \\
	\hline 
    MSE  &  &  0.29 &  0.29 &  0.26 &  0.25 &  &  0.02 &  0.02 &  0.03 &  0.00 \\
    MAE  &  &  0.33 &  0.33 &  0.32 &  0.31 &  &  0.16 &  0.16 &  0.16 &  0.17 \\
    RMSE &  &  0.43 &  0.43 &  0.41 &  0.40 &  &  0.28 &  0.28 &  0.29 &  0.28 \\
    SDE  &  &  0.31 &  0.31 &  0.31 &  0.32 &  &  0.29 &  0.29 &  0.29 &  0.29 \\
    Min  &  & -0.29 & -0.29 & -0.31 & -0.34 &  & -0.85 & -0.85 & -0.79 & -0.82 \\
    Max  &  &  1.56 &  1.56 &  1.56 &  1.55 &  &  1.15 &  1.15 &  1.16 &  1.14 \\
    \end{tabular}
  \end{ruledtabular}
\end{table*}

\end{document}
