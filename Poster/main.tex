\documentclass[25pt, a0paper, portrait]{tikzposter}
\usepackage{blindtext}
\usepackage{comment}
\usepackage{adjustbox}
\usepackage{graphicx,dcolumn,bm,xcolor,microtype,multirow,amscd,amsmath,amssymb,amsfonts,physics,longtable,wrapfig,bbold,siunitx,xspace}

% \usetheme{Desert}
% \usecolorstyle{Britain}
\usetitlestyle{VerticalShading}
\useblockstyle{Slide}
\usenotestyle{VerticalShading}

\usetikzlibrary{positioning}
\title{\parbox{0.7\linewidth}{\centering A similarity renormalization group approach to Green's function methods}}
% \title{A similarity renormalization group approach \\ to Green's function methods}
\author{Antoine MARIE and Pierre-François \textsc{LOOS}}
\date{\today}
\institute{Laboratoire de Chimie et Physique Quantiques (UMR 5626), Université de Toulouse, CNRS, UPS, France}

\definecolor{darkgreen}{RGB}{0, 180, 0}
\definecolor{fooblue}{RGB}{0,153,255}
\definecolor{fooyellow}{RGB}{234,180,0}
\definecolor{lavender}{rgb}{0.71, 0.49, 0.86}
\definecolor{inchworm}{rgb}{0.7, 0.93, 0.36}
\definecolor{footer}{cmyk}{0.739,0.288,0,0.278}
\newcommand{\violet}[1]{\textcolor{violet}{#1}}
\newcommand{\orange}[1]{\textcolor{orange}{#1}}
\newcommand{\purple}[1]{\textcolor{purple}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\teal}[1]{\textcolor{teal}{#1}}
\newcommand{\green}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\yellow}[1]{\textcolor{fooyellow}{#1}}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\cyan}[1]{\textcolor{cyan}{#1}}
\newcommand{\magenta}[1]{\textcolor{magenta}{#1}}
\newcommand{\highlight}[1]{\textcolor{fooblue}{#1}}
\newcommand{\pub}[1]{\textcolor{purple}{#1}}

\newcommand{\bSig}{\boldsymbol{\Sigma}}
\newcommand{\bSigC}{\boldsymbol{\Sigma}^{\text{c}}}
\newcommand{\be}{\boldsymbol{\epsilon}}
\newcommand{\bOm}{\boldsymbol{\Omega}}
\newcommand{\bEta}[1]{\boldsymbol{\eta}^{(#1)}(s)}

\newcommand{\ii}{\mathrm{i}}
\newcommand{\GW}{GW}
\newcommand{\GF}{\text{GF(2)}}
\newcommand{\GT}{GT}	
\newcommand{\evGW}{\text{ev}GW}	
\newcommand{\qsGW}{\text{qs}GW}
\newcommand{\SRGGW}{\text{SRG-}GW}
\newcommand{\SRGqsGW}{\text{SRG-qs}GW}
\newcommand{\GOWO}{G_0W_0}

\makeatletter
\newcommand\insertlogoi[2][]{\def\@insertlogoi{\includegraphics[#1]{#2}}}
\newcommand\insertlogoii[2][]{\def\@insertlogoii{\includegraphics[#1]{#2}}}
\newcommand\insertlogoiii[2][]{\def\@insertlogoiii{\includegraphics[#1]{#2}}}
\newcommand\insertlogoiv[2][]{\def\@insertlogoiv{\includegraphics[#1]{#2}}}
\newlength\LogoHSep
\newlength\LogoVSep

\setlength\LogoHSep{-1cm}
\setlength\LogoVSep{1.cm}

\insertlogoi[width=7.5cm]{CNRS}
%\insertlogoii[width=5cm]{images/overleaf-logo}
%\insertlogoiii[width=5cm]{images/overleaf-logo}
\insertlogoiv[width=7.5cm]{ERC}

\renewcommand\maketitle[1][]{  % #1 keys
    \normalsize
    \setkeys{title}{#1}
    % Title dummy to get title height
    \node[transparent,inner sep=\TP@titleinnersep, line width=\TP@titlelinewidth, anchor=north, minimum width=\TP@visibletextwidth-2\TP@titleinnersep]
        (TP@title) at ($(0, 0.5\textheight-\TP@titletotopverticalspace)$) {\parbox{\TP@titlewidth-2\TP@titleinnersep}{\TP@maketitle}};
    \draw let \p1 = ($(TP@title.north)-(TP@title.south)$) in node {
        \setlength{\TP@titleheight}{\y1}
        \setlength{\titleheight}{\y1}
        \global\TP@titleheight=\TP@titleheight
        \global\titleheight=\titleheight
    };

    % Compute title position
    \setlength{\titleposleft}{-0.5\titlewidth}
    \setlength{\titleposright}{\titleposleft+\titlewidth}
    \setlength{\titlepostop}{0.5\textheight-\TP@titletotopverticalspace}
    \setlength{\titleposbottom}{\titlepostop-\titleheight}

    % Title style (background)
    \TP@titlestyle

    % Title node
    \node[inner sep=\TP@titleinnersep, line width=\TP@titlelinewidth, anchor=north, minimum width=\TP@visibletextwidth-2\TP@titleinnersep]
        at (0,0.5\textheight-\TP@titletotopverticalspace)
        (title)
        {\parbox{\TP@titlewidth-2\TP@titleinnersep}{\TP@maketitle}};

     \node[inner sep=0pt,anchor=west]
      at ([shift={(-\LogoHSep,\LogoVSep)}]title.west)
      (logo1)
      {\@insertlogoi};

    % \node[inner sep=0pt,anchor=west,right=of logo1] 
    %  (logo2)
    %  {\@insertlogoii};

    \node[inner sep=0pt,anchor=east] 
      at ([shift={(\LogoHSep,\LogoVSep)}]title.east)
      (logo4)
      {\@insertlogoiv};

    % \node[inner sep=0pt,left=of logo4] 
    %  (logo4)
    %  {\@insertlogoiii};

    % Settings for blocks
    \normalsize
    \setlength{\TP@blocktop}{\titleposbottom-\TP@titletoblockverticalspace}
}
\makeatother


\begin{document}

\maketitle

\begin{columns}
    \column{0.5}
    \block{Dynamic $GW$}
    {
    \begin{minipage}{0.4\linewidth}
      \begin{tikzfigure}
        \includegraphics[width=0.8\textwidth]{square}
      \end{tikzfigure}
    \end{minipage}
    \begin{minipage}{0.6\linewidth}
      \begin{equation*}
	\qty[ \underbrace{\blue{\boldsymbol{F}}}_{\text{\blue{Fock matrix}}} + \underbrace{\violet{\boldsymbol{\Sigma}^{\GW}} (\omega = \teal{\epsilon^{GW}_{p}})}_{\text{\violet{dynamic self-energy}}} ] \psi_{p}^{GW} 
	 = \teal{\epsilon^{GW}_{p}} \psi_{p}^{GW}
       \end{equation*}
       \vspace{1cm}
       \begin{equation*}
         \begin{split}
	\violet{\Sigma_{pq}^{GW}}(\omega) 
			&= \sum_{i\nu} \frac{\red{W_{pi}^{\nu}} \red{W_{qi}^{\nu}}}{\omega - \teal{\epsilon^{GW}_{i}} + \orange{\Omega_{\nu}} - \ii \eta} \\
           &+ \sum_{a\nu} \frac{\red{W_{pa}^{\nu}} \red{W_{qa}^{\nu}}}{\omega - \teal{\epsilon^{GW}_{a}} - \orange{\Omega_{\nu}} + \ii \eta}
         \end{split}
       \end{equation*}
     \end{minipage}
     
     \vspace{0.5cm}
     \small L. Hedin, Phys. Rev. 139, A796 (1965); R. M. Martin, L. Reining, and D. M. Ceperley, (Cambridge University Press, 2016)
    }
    \column{0.5}
    \block{Similarity Renormalization Group (SRG)}{
    \begin{minipage}{0.49\linewidth}
   SRG flow equation
    \begin{equation}
        \label{eq:flowEquation}
        \dv{\boldsymbol{H}(s)}{s} = \comm{\boldsymbol{\eta}(s)}{\boldsymbol{H}(s)}
    \end{equation}
    Similarity transformed Hamiltonian
    \begin{equation}
        \label{eq:SRG_Ham}
        \boldsymbol{H}(s) = \boldsymbol{U}(s) \, \boldsymbol{H} \, \boldsymbol{U}^\dagger(s)
    \end{equation}
    Wegner generator
    %\begin{equation}
    %    \boldsymbol{\eta}(s) = \dv{\boldsymbol{U}(s)}{s}	\boldsymbol{U}^\dagger(s)	= - \boldsymbol{\eta}^\dag(s)
    %\end{equation}
    \begin{equation}
  \boldsymbol{\eta}^\text{W}(s) = \comm{\boldsymbol{H}^\text{d}(s)}{\boldsymbol{H}^\text{od}(s)}
\end{equation}

\vspace{0.5cm}
\small F. Wegner, Ann. Phys. 3, 77 (1994) \\
S. D. Głazek and K. G. Wilson, Phys. Rev. D 48, 5863 (1993)
    \end{minipage}
    \hfill\vline\hfill
    \begin{minipage}{0.49\linewidth}
      \begin{tikzfigure}
        \includegraphics[width=1.\textwidth]{SRGMatrix}
      \end{tikzfigure}
    \end{minipage}
    }
\end{columns}

\begin{columns}
  \column{0.35}
  \block{Static $GW$}
  {
    \begin{tikzfigure}
      \includegraphics[width=0.27\textwidth]{upfolding.pdf}
    \end{tikzfigure}
  \small S. J. Bintrim and T. C. Berkelbach, J. Chem. Phys. 154, 041101
(2021).}
  \column{0.65}
  \block{SRG-$GW$}
  {
    \begin{minipage}{0.6\linewidth}
      \begin{equation*}
        \begin{split}
  	  \blue{\widetilde{\boldsymbol{F}}_{pq}}(s) &= \delta_{pq} \blue{\epsilon^{\text{HF}}_{p}} + \sum_{r\nu} \frac{\Delta_{pr}^{\nu} + \Delta_{qr}^{\nu}}{(\Delta_{pr}^{\nu})^2 + (\Delta_{qr}^{\nu})^2 } \red{W_{pr}^{\nu}} \red{W_{qr}^{\nu}} \qty[1  - e^{-((\Delta_{pr}^{\nu})^2+(\Delta_{qr}^{\nu})^2) s} ] \\
          \\
          \Delta_{pr}^{\nu} &= \teal{\epsilon^{GW}_{p}} - \teal{\epsilon^{GW}_{r}} \pm \orange{\Omega_{\nu}} \\
          \\
          \violet{\widetilde{\Sigma}_{pq}^{\SRGGW}} &= \sum_{i\nu} \frac{e^{-(\Delta_{pi}^{\nu})^2 s}\red{W_{pi}^{\nu}} \red{W_{qi}^{\nu}}e^{-(\Delta_{qi}^{\nu})^2 s}}{\omega - \teal{\epsilon^{GW}_{i}} + \orange{\Omega_{\nu}}} + \sum_{a\nu} \frac{e^{-(\Delta_{pa}^{\nu})^2 s}\red{W_{pa}^{\nu}} \red{W_{qa}^{\nu}}e^{-(\Delta_{qa}^{\nu})^2 s}}{\omega - \teal{\epsilon^{GW}_{a}} - \orange{\Omega_{\nu}}}
        \end{split}
      \end{equation*}
    \end{minipage}
    \begin{minipage}{0.4\linewidth}
      \begin{tikzfigure}
        \includegraphics[width=\textwidth]{fig1.pdf}
      \end{tikzfigure}
    \end{minipage}
  }
  
\end{columns}

\block{Functional form of the qs$GW$ and SRG-qs$GW$}
{
  \begin{adjustbox}{valign=t}
    \begin{minipage}[t]{0.25\linewidth}
      \begin{tikzfigure}
        \includegraphics[width=0.82\textwidth]{fig2a.pdf}
      \end{tikzfigure}
    \end{minipage}
  \end{adjustbox}
  \begin{minipage}[t]{0.5\linewidth}
    \begin{itemize}
    \bigskip
    \item qs$GW$ self-energy:
    \bigskip
    \begin{equation*}
        \boldsymbol{\Sigma}^{\text{qs}GW}_{pq}(\eta) = 
        \sum_{r\nu} \frac{1}{2}\qty(\frac{\Delta_{pr}^{\nu}}{(\Delta_{pr}^{\nu})^2 + \eta^2 } + \frac{\Delta_{qr}^{\nu}}{(\Delta_{qr}^{\nu})^2 + \eta^2}) W_{pr}^{\nu} W_{qr}^{\nu}
    \end{equation*}
    \bigskip
    {\small S. V. Faleev, M. van Schilfgaarde, and T. Kotani, Phys. Rev. Lett. 93, 126406 (2004)}
    \bigskip
    \item SRG-qs$GW$ self-energy:
    \bigskip
    \begin{equation*}
        \boldsymbol{\Sigma}^{\text{SRG-qs}GW}_{pq}(s) = 
        \sum_{r\nu} \frac{\Delta_{pr}^{\nu} + \Delta_{qr}^{\nu}}{(\Delta_{pr}^{\nu})^2 + (\Delta_{qr}^{\nu})^2 } W_{pr}^{\nu} W_{qr}^{\nu} \qty[1  - e^{-((\Delta_{pr}^{\nu})^2+(\Delta_{qr}^{\nu})^2) s} ]
    \end{equation*}
    \bigskip
    {\small A. Marie and P.-F. Loos, arXiv:2303.05984 (2023)}
    \end{itemize}
    \bigskip
  \end{minipage}
  \begin{adjustbox}{valign=t}
    \begin{minipage}[t]{0.25\linewidth}
      \begin{tikzfigure}
        \includegraphics[width=0.82\textwidth]{fig2b.pdf}
      \end{tikzfigure}
    \end{minipage}
  \end{adjustbox}
%  \begin{minipage}[t]{0.26\linewidth}
%    \vspace{3cm}
%    \begin{equation*}
%      \begin{split}
%        &\boldsymbol{\Sigma}^{\text{SRG-qs}GW}_{pq}(s) = \\
%        \\
%        &\sum_{r\nu} \frac{\Delta_{pr}^{\nu} + \Delta_{qr}^{\nu}}{(\Delta_{pr}^{\nu})^2 + (\Delta_{qr}^{\nu})^2 } W_{pr}^{\nu} W_{qr}^{\nu} \qty[1  - e^{-((\Delta_{pr}^{\nu})^2+(\Delta_{qr}^{\nu})^2) s} ]
%      \end{split}
%    \end{equation*}
%  \end{minipage}
}

\begin{columns}
    \column{0.33}
    \block{IP flow parameter dependence}{
        \begin{tikzfigure}
            \includegraphics[height=13.5cm]{fig3.pdf}
          \end{tikzfigure}}
    \column{0.33}
    \block{EA flow parameter dependence}{
        \begin{tikzfigure}
            \includegraphics[height=13.5cm]{fig4.pdf}
        \end{tikzfigure}}
    \column{0.33}
    \block{MAE flow parameter dependence}{    
        \begin{tikzfigure}
            \includegraphics[height=13.5cm]{fig6.pdf}
        \end{tikzfigure}}

    \end{columns}
    

\begin{columns}
    \column{0.85}
\block{$GW$50 statistics}{    
        \begin{tikzfigure}
            \includegraphics[height=13.5cm]{fig5.pdf}
        \end{tikzfigure}}
\column{0.15}
\block{Funding}{    
This project has received funding from the European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme (Grant agreement No. 863481).}
\end{columns}

\node [above right,outer sep=0pt,minimum width=\paperwidth,align=left,draw,fill=footer] at (bottomleft) {\Large \textcolor{white}{\textbf{amarie@irsamc.ups-tlse.fr} }};

\end{document}
