\documentclass[9pt,aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{mathtools,amsmath,amssymb,amsfonts,graphicx,xcolor,bm,microtype,wasysym,hyperref,tabularx,amscd,mhchem,physics}


\definecolor{darkgreen}{RGB}{0, 180, 0}
\definecolor{fooblue}{RGB}{0,153,255}
\definecolor{fooyellow}{RGB}{234,180,0}
\definecolor{lavender}{rgb}{0.71, 0.49, 0.86}
\definecolor{inchworm}{rgb}{0.7, 0.93, 0.36}
\newcommand{\violet}[1]{\textcolor{lavender}{#1}}
\newcommand{\orange}[1]{\textcolor{orange}{#1}}
\newcommand{\purple}[1]{\textcolor{purple}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\green}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\yellow}[1]{\textcolor{fooyellow}{#1}}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\highlight}[1]{\textcolor{fooblue}{#1}}
\newcommand{\pub}[1]{\textcolor{purple}{#1}}

\newcommand{\bC}{\boldsymbol{C}}
\newcommand{\bD}{\boldsymbol{D}}
\newcommand{\bF}{\boldsymbol{F}}
\newcommand{\bH}{\boldsymbol{H}}
\newcommand{\bHd}{\boldsymbol{H}_\text{d}}
\newcommand{\bHod}{\boldsymbol{H}_\text{od}}
\newcommand{\bO}{\boldsymbol{0}}
\newcommand{\bI}{\boldsymbol{1}}
\newcommand{\bU}{\boldsymbol{U}}
\newcommand{\bV}{\boldsymbol{V}}
\newcommand{\bW}{\boldsymbol{W}}
\newcommand{\bEta}{\boldsymbol{\eta}}
\newcommand{\bSig}{\boldsymbol{\Sigma}}
\newcommand{\bpsi}{\boldsymbol{\psi}}
\newcommand{\bPsi}{\boldsymbol{\Psi}}

\newcommand{\la}{\lambda}
\newcommand{\om}{\omega}
\newcommand{\Om}{\Omega}
\newcommand{\eps}{\varepsilon}

\institute{Laboratoire de Chimie et Physique Quantiques, IRSAMC, UPS/CNRS, Toulouse\\
\url{https://lcpq.github.io/pterosor}}
\usetheme{pterosor}
\author{Antoine Marie \& Pierre-Fran\c{c}ois Loos}
\date{14th November 2022}
\title{A Similarity Renormalization Group (SRG) Approach to Green's Function Methods}

\begin{document}

\maketitle

%-----------------------------------------------------
\begin{frame}{First-Quantized Form of SRG}
	\begin{block}{General upfolded/downfolded many-body perturbation theory (MBPT) problem}
    	\begin{equation}
	     	\left.
			\begin{array}{cc}
    		\qty[ \bF + \bSig(\om) ] \bpsi = \om \bpsi
			\\
			\\
			\bSig(\om) = \bV \qty(\om \bI - \bC)^{-1} \bV^{\dag} 
			\end{array}
			\right\}
			\qq{$\xleftrightharpoons[upfolding]{downfolding}$}
			\begin{cases}
				\bH \bPsi = \om \bPsi
    			\\
       			\bH =
            		\begin{pmatrix}
            			\bF			&	\bV
            			\\
            			\bV^{\dagger}	&	\bC
            		\end{pmatrix}
			\end{cases}	
    	\end{equation}	
	\end{block}	
	%
	\begin{block}{Perturbative partitioning (one choice at least)}
        \begin{equation}
        	\bH =
            \underbrace{
            \begin{pmatrix}
            	\bF & \bO 
				\\
            	\bO		& \bC
            \end{pmatrix}
            }_{\bHd}
            + \la
			\underbrace{
			\begin{pmatrix}
				\bO				& \bV
				\\
            	\bV^{\dagger}	& \bO
			\end{pmatrix}
			}_{\bHod}
       \end{equation}
	\end{block}
\end{frame}
%-----------------------------------------------------

%-----------------------------------------------------
\begin{frame}{Perturbative Expansions}
	%
       %  \begin{block}{Perturbative partitioning in the SRG framework}
       %  \begin{equation}
       %  	\bH(s) =
       %      \underbrace{
       %      \begin{pmatrix}
       %      	\bF(s) & \bO 
       %  			\\
       %      	\bO		& \bC(s) 
       %      \end{pmatrix}
       %      }_{\bHd{}(s)}
       %      + \la
       %  		\underbrace{
       %  		\begin{pmatrix}
       %  			\bO				& \bV(s) 
       %  			\\
       %      	\bV^{\dagger}(s)	& \bO
       %  		\end{pmatrix}
       %  		}_{\bHod(s)}
       % \end{equation}
       %  \end{block}
	\begin{block}{Components of the effective Hamiltonian}
          \begin{subequations}
            \begin{align}
              \bH(s) & = \bH^{(0)}(s) + \la \bH^{(1)}(s) + \la^2 \bH^{(2)}(s) + \cdots
              \\
              \bF(s) &= \bF^{(0)}(s) + \la \bF^{(1)}(s) + \la^2 \bF^{(2)}(s) + \cdots
              \\
              \bC(s) & = \bC^{(0)}(s) + \la \bC^{(1)}(s) + \la^2 \bC^{(2)}(s) + \cdots
              \\
              \bV(s) & = \bV^{(0)}(s) + \la \bV^{(1)}(s) + \la^2 \bV^{(2)}(s) + \cdots
            \end{align}	
          \end{subequations}	
 	\end{block}
	\begin{block}{Wegner generator}
          \begin{equation}
            \bEta(s) 
            = \comm{\bHd(s)}{\bHod(s)} 
            = \bEta^{(0)}(s) + \la \bEta^{(1)}(s) + \la^2 \bEta^{(2)}(s) + \cdots
          \end{equation}	
 	\end{block}
\end{frame}
%-----------------------------------------------------

%-----------------------------------------------------
\begin{frame}{Zeroth-Order Terms}
  \begin{block}{Wegner generator}
    \begin{equation}
      \bEta^{(0)}(s) 
      = \comm{\bHd^{(0)}(s)}{\bHod^{(0)}(s)} 
      = \bO
      \qq{because}
      \bHod^{(0)}(s) = \bO
    \end{equation}	
  \end{block}
	%
	\begin{block}{Zeroth-order effective Hamiltonian}
        \begin{equation}
			\dv{\bH^{(0)}(s)}{s} 
			= \comm{\bEta^{(0)}(s)}{\bH^{(0)}(s)} 
			= \bO
			\qq{$\Rightarrow$} 
			\boxed{\bH^{(0)}(s) = \bH^{(0)}{(0)} = \bHd(0)}
       \end{equation}	
 	\end{block}
	\alert{NB: we omit the $s$ dependency from hereon}
\end{frame}
%-----------------------------------------------------

%-----------------------------------------------------
\begin{frame}{First-Order Terms}
	\begin{block}{Wegner generator}
        \begin{equation}
			\bEta^{(1)} 
			= \comm{\bHd^{(0)}}{\bHod^{(1)}} 
			+ \underbrace{\comm{\bHd^{(1)}}{\bHod^{(0)}}}_{\bHod^{(0)} = \bHd^{(1)} = \bO}
			= 
			\begin{pmatrix}
				\bO & \bF^{(0)} \bV^{(1)} - \bV^{(1)} \bC^{(0)}
				\\
				\bC^{(0)} \bV^{(1),\dagger} - \bV^{(1),\dagger} \bF^{(0)} & \bO
 			 \end{pmatrix}         
  		\end{equation}	
 	\end{block}
	%
	\begin{block}{First-order effective Hamiltonian}
		\begin{equation}
			\dv{\bH^{(1)}}{s} 
			= \comm{\bEta^{(0)}}{\bH^{(1)}} 
			+ \comm{\bEta^{(1)}}{\bH^{(0)}} 
			= \comm{\bEta^{(1)}}{\bHd^{(0)}} 
			=
			\begin{pmatrix}
				\dv{\bF^{(1)}}{s} & \dv{\bV^{(1)}}{s} 
				\\
				\dv{\bV^{(1),\dagger}}{s} & \dv{\bC^{(1)}}{s}
			\end{pmatrix} 
		\end{equation}
		with
		\begin{gather}
			\dv{\bF^{(1)}}{s} 
			= \dv{\bC^{(1)}}{s} 
			= \bO
			\\
			\dv{\bV^{(1)}}{s} 
			= 2 \bF^{(0)} \bV^{(1)} \bC^{(0)}
			- \qty[\bF^{(0)}]^2 \bV^{(1)} 
			- \bV^{(1)} \qty[\bC^{(0)}]^2
		\end{gather}
	\end{block}
\end{frame}
%-----------------------------------------------------

%-----------------------------------------------------
\begin{frame}{Integration of the First-Order Terms}
	\begin{block}{Diagonal terms}
		\begin{equation}
			\dv{\bF^{(1)}}{s} = \bO 
			\Leftrightarrow
			\bF^{(1)}(s) = \bF^{(1)}(0) 
                        \Leftrightarrow
			\boxed{\bF^{(1)}(s) = \bO}
		\end{equation}
		\begin{equation}
			\dv{\bC^{(1)}}{s} = \bO 
			\Leftrightarrow
			\bC^{(1)}(s) = \bC^{(1)}(0) 
                        \Leftrightarrow
			\boxed{\bC^{(1)}(s) = \bO}
		\end{equation}
	\end{block}
	\pause[2]
	\begin{block}{Off-diagonal terms}
		\begin{gather}
			\dv{\bV^{(1)}}{s} 
			= 2 \bF^{(0)} \bV^{(1)} \bC^{(0)}
			- \qty[\bF^{(0)}]^2 \bV^{(1)} 
			- \bV^{(1)} \qty[\bC^{(0)}]^2
			\\
                        \dv{\bW^{(1)}}{s} = 2 \bF^{(0)}\bW^{(1)} \bD^{(0)} - \qty[\bF^{(0)}]^2\bW^{(1)} - \bW^{(1)} \qty[\bD^{(0)}]^2 \\
                        \qq{with} \bW^{(1)} = \bV^{(1)} \bU  \qq{and} \bC^{(0)} = \bU \bD^{(0)} \bU^{\dag}\\
			\Rightarrow 
			\boxed{W^{(1)}_{pq,m}(s) = W_{pq,m}^{(1)}(0)e^{-(\Delta_{pq}^{m})^2 s} 
			\qq{and} 
			\Delta_{pq}^{m} = \epsilon_p - \epsilon_q + \Om_m \text{sgn}(\mu - \eps_q)
			}
		\end{gather}
	\end{block}
\end{frame}
%-----------------------------------------------------

%-----------------------------------------------------
\begin{frame}{Second-Order Terms}
	\begin{block}{Wegner generator}
        \begin{equation}
			\bEta^{(2)}
			= \comm{\bHd^{(0)}}{\bHod^{(2)}}
			+ \underbrace{\comm{\bHd^{(1)}}{\bHod^{(1)}}}_{\bO}  
			+ \underbrace{\comm{\bHd^{(2)}}{\bHod^{(0)}}}_{\bO}  
			= 
			\begin{pmatrix}
				\bO & \bF^{(0)} \bV^{(2)} - \bV^{(2)} \bC^{(0)}
				\\
				\bC^{(0)} \bV^{(2),\dagger} - \bV^{(2),\dagger} \bF^{(0)} & \bO
 			 \end{pmatrix}      
  		\end{equation}	
 	\end{block}
	%
	\begin{block}{Second-order effective Hamiltonian}
		\begin{equation}
			\dv{\bH^{(2)}}{s} 
			= \comm{\bEta^{(2)}}{\bHd^{(0)}} 
			+ \comm{\bEta^{(1)}}{\bHod^{(1)}}
			+ \comm{\bEta^{(0)}}{\bH^{(2)}} 
			=
			\begin{pmatrix}
				\dv{\bF^{(2)}}{s} & \dv{\bV^{(2)}}{s} 
				\\
				\dv{\bV^{(2),\dagger}}{s} & \dv{\bC^{(2)}}{s}
			\end{pmatrix} 
		\end{equation}
		\begin{align}
			\dv{\bF^{(2)}}{s} 
			& = \bF^{(0)} \bV^{(1)} \bV^{(1),\dag} 
			+ \bV^{(1)} \bV^{(1),\dag} \bF^{(0)} 
			- 2 \bV^{(1)} \bC^{(0)} \bV^{(1),\dag}
			\\
			\dv{\bC^{(2)}}{s} 
			& = \bC^{(0)} \bV^{(1),\dag} \bV^{(1)} 
			+ \bV^{(1),\dag} \bV^{(1)} \bC^{(0)} 
			- 2 \bV^{(1),\dag} \bF^{(0)} \bV^{(1)}
			\\
			\dv{\bV^{(2)}}{s} 
			& = 2 \bF^{(0)} \bV^{(2)} \bC^{(0)}
			- \qty[\bF^{(0)}]^2 \bV^{(2)} 
			- \bV^{(2)} \qty[\bC^{(0)}]^2
		\end{align}
	\end{block}
\end{frame}
%-----------------------------------------------------


%-----------------------------------------------------
\begin{frame}{Integration of the Second-Order Terms}
	\begin{block}{Diagonal terms}
		\begin{gather}
			\dv{\bF^{(2)}}{s} 
			= \bF^{(0)} \bV^{(1)} \bV^{(1),\dag} 
			+ \bV^{(1)} \bV^{(1),\dag} \bF^{(0)} 
			- 2 \bV^{(1)} \bC^{(0)} \bV^{(1),\dag}
			\\
			\Rightarrow 
			F_{pq}^{(2)}(s) 
			= \sum_{rm} \frac{\Delta_{pr}^{m} + \Delta_{qr}^{m}}{(\Delta_{pr}^{m})^2 + (\Delta_{qr}^{m})^2 } 
			W_{pr,m}^{(1)}(0) W_{qr,m}^{(1)}(0) \qty[ 1 - e^{-(\Delta_{pr}^{m})^2s} e^{-(\Delta_{qr}^{m})^2s} ] 
		\end{gather}
              \end{block}
              \pause[2]
	\begin{block}{Off-diagonal terms}
		\begin{equation}
			\dv{\bV^{(2)}}{s} 
			= 2 \bF^{(0)} \bV^{(2)} \bC^{(0)}
			- \qty[\bF^{(0)}]^2 \bV^{(2)} 
			- \bV^{(2)} \qty[\bC^{(0)}]^2
			\Rightarrow 
			\boxed{\bV^{(2)}(s) = \bO}
		\end{equation}
	\end{block}
\end{frame}
%-----------------------------------------------------

%-----------------------------------------------------
\begin{frame}{Regularized Quasiparticle Equation}
  \begin{block}{Regularized $GW$ equations up to second order}
    \begin{equation}
      \qty[ \Tilde{\bF}(s) + \Tilde{\bSig}(\om;s) ] \bpsi = \om \bpsi
    \end{equation}
  \end{block}
  \pause[2]
  \begin{block}{Regularized Fock elements}
		\begin{equation}
				\Tilde{\bF}(s) = \bF + \bF^{(2)}(s)
                \qq{with}
    			\Tilde{\bF}_{pq}(s) = \delta_{pq} \eps_{p} 
				+ \sum_{rm} \frac{\Delta_{pr}^{m} + \Delta_{qr}^{m}}{(\Delta_{pr}^{m})^2 + (\Delta_{qr}^{m})^2 }  
				\qty[ \Tilde{W}_{pr,m}(0) \Tilde{W}_{qr,m}(0) - \Tilde{W}_{pr,m}(s) \Tilde{W}_{qr,m}(s) ]
		\end{equation}
	\end{block}
	\begin{block}{Regularized $GW$ self-energy}
		\begin{equation}
    		\Tilde{\Sigma}_{pq}(\om;s) 
			= \sum_{im} \frac{\Tilde{W}_{pi,m}(s) \Tilde{W}_{qi,m}(s)}{\om - \eps_{i} + \Om_{m}}
			+ \sum_{am} \frac{\Tilde{W}_{pa,m}(s) \Tilde{W}_{qa,m}(s)}{\om - \eps_{a} - \Om_{m}}
			\qq{with}
			\Tilde{W}_{pq,m}(s) = W_{pq,m} e^{-(\Delta_p^{qm})^2 s}
		\end{equation}
	\end{block}
\end{frame}
%-----------------------------------------------------

%-----------------------------------------------------
\begin{frame}{Limiting Forms}
	\begin{block}{Limit as $s \to 0$}
		\begin{equation}
    		\bF^{(2)}(s = 0) = \bO 
			\qq{$\Rightarrow$}
			\Tilde{\bF}(s=0) = \bF
 			\qq{and}
   			 \Tilde{\bSig}(\om;s=0) = \bSig(\om)
		\end{equation}
	\end{block}
	\begin{block}{Limit as $s \to \infty$}
		\begin{equation}
			\Tilde{\bSig}(\om;s\to\infty) = \bO
			\qq{and}
			\Tilde{F}_{pq}(s\to\infty) 
			= \delta_{pq} \eps_{p}
			+ \underbrace{\sum_{rm} \frac{\Delta_{pr}^{m} + \Delta_{qr}^{m}}{(\Delta_{pr}^{m})^2 + (\Delta_{qr}^{m})^2 } 
			W_{pr,m}^{(1)}(0) W_{qr,m}^{(1)}(0)}_{\text{static correction}}
		\end{equation}
	\end{block}
	\alert{By removing the coupling terms, SRG transforms continuously the dynamical problem into a static one}
\end{frame}
%-----------------------------------------------------

%-----------------------------------------------------
%\begin{frame}{Integration of the Second-Order Terms}
%	\begin{block}{Diagonal terms}
%		\begin{gather}
%			\dv{\bC^{(2)}}{s} 
%			= \bC^{(0)} \bV^{(1)} \bV^{(1),\dag} 
%			+ \bV^{(1)} \bV^{(1),\dag} \bC^{(0)} 
%			- 2 \bV^{(1)} \bF^{(0)} \bV^{(1),\dag}
%			\\
%			\Rightarrow 
%			C_{(r,v),(s,t)}^{(2)}(s)  = \sum_{p} \frac{-\Delta_p^{(r,v)} - \Delta_p^{(s,t)}}{(\Delta_p^{(r,v)})^2 + (\Delta_p^{(s,t)})^2 } W_{p,(r,v)}^{(1)}(0)W_{p,(s,t)}^{(1)}(0)\qty(1-e^{-(\Delta_{p}^{(r,v)})^2s} e^{-(\Delta_{p}^{(s,t)})^2s}) 
%		\end{gather}
%	\end{block}
%\end{frame}

%-----------------------------------------------------


\end{document}
